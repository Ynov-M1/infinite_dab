import 'package:flutter/material.dart';

class Store {
  final String title;
  List<Bonus> contents = [];
  final IconData icon;

  Store(this.title, this.contents, this.icon);
}

class DabStatus {
  int dabPowerClick;
  int autoDabCount;
  int dabCount;

  DabStatus(this.dabPowerClick, this.autoDabCount, this.dabCount);
}

class Bonus {
  final String title;
  int hitPerSecond;
  int dabPowerClick;
  int price;
  int purchased = 0;

  Bonus(this.title, this.hitPerSecond, this.dabPowerClick, this.price);
}
