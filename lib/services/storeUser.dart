import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter/material.dart';

class StoreUser {
  final appDirectory = _localPath;

  static String get _localPath  {
    getApplicationDocumentsDirectory().then((directory) {
      return directory.path;
    });

  }

  File get _localFile  {
    File returnFile;
    if(File('$appDirectory/user.txt') == null) {
      returnFile = new File('$appDirectory/user.txt');
    } else {
      returnFile = File('$appDirectory/user.txt');
    }
    return returnFile;
  }

  File getFile (String path) {
    File returnFile;
    if(File('$path/user.txt') == null) {
      returnFile = new File('$path/user.txt');
    } else {
      returnFile = File('$path/user.txt');
    }
    return returnFile;
  }

  void writeFile(String string) {
    getApplicationDocumentsDirectory().then((directory) {
      File file = getFile(directory.path);
      file.writeAsStringSync(string);
    });
  }

  String readFile(File _localeFile) {
    try {
      return _localeFile.readAsStringSync();
      // Read the file

    } catch (e) {
      print(e);
      // If encountering an error, return 0
      return 'NO_USER';
    }
  }


}