import 'package:firebase_database/firebase_database.dart';
import 'package:infinite_dab/services/storeUser.dart';
import 'package:infinite_dab/objects/store.dart';


class DbHandler {
  final databaseReference = FirebaseDatabase.instance.reference();

//  String get _user {
//    StoreUser storeUser = new StoreUser();
//    return storeUser.readFile();
//  }
  void createRecord(int score, int dabPowerClick, String user) async{
    databaseReference.child(user).set({
      'score': score,
      'dabPowerClick': dabPowerClick,
    });
  }

  DatabaseReference getDbReference(){
    return databaseReference;
  }

  int getData(String userId) {
    databaseReference.reference().child(userId).once().then((
        DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      return map.values.toList()[0];
    });
  }
}