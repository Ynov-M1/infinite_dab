import 'package:flutter/material.dart';
import 'dart:io';
import "package:flare_flutter/flare_actor.dart";
import 'package:firebase_database/firebase_database.dart';
import 'package:path_provider/path_provider.dart';

import 'package:infinite_dab/login.dart';
import 'package:infinite_dab/services/authentication.dart';
import 'package:infinite_dab/services/storeUser.dart';
import 'package:infinite_dab/services/database.dart';
import 'package:infinite_dab/objects/store.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Infinite Dab',
      home: MyHomePage(title: 'Infinite DAB'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}


List<Store> stores = [
  new Store(
    'DabPower',
    [
      new Bonus('DabPowerClick I', 0, 1, 5),
      new Bonus('DabPowerClick II', 0, 3, 20),
      new Bonus('DabPowerClick III', 0, 5, 150),
      new Bonus('DabPowerClick IV', 0, 15, 200)
    ],
    Icons.camera,
  ),
];

class _MyHomePageState extends State<MyHomePage> {
  DabStatus dabStatus = new DabStatus(1, 0, 0);
  double width = 0;
  DbHandler dbHandler = new DbHandler();
  StoreUser storeUser = new StoreUser();
  String currentAnimation = "";
  bool isAnimating = false;
  int _counter = 0;
  int dabPowerClick = 1;
  String _userId;
  bool isLogged = false;

  _MyHomePageState() {
    getApplicationDocumentsDirectory().then((directory) {
      File file = storeUser.getFile(directory.path);
      _userId = storeUser.readFile(file);
      if(_userId != "") {
        initializeCounter();
      }

    });
  }

  void logout(){
    storeUser.writeFile("");
    setState(() {
      isLogged = false;
      _counter = 0;
      dabPowerClick = 1;
    });
  }

  void initializeCounter() {
    final databaseReference = dbHandler.getDbReference();
    databaseReference.reference().child(_userId).once().then((
        DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      setState(() {
        _counter = map.values.toList()[0];
        dabPowerClick = map.values.toList()[1];
        isLogged = true;
      });
    });
  }

  void _incrementCounter() {
    setState(() {
      _counter += dabPowerClick;
      if(isLogged) {
        dbHandler.createRecord(_counter, dabPowerClick, _userId);
      }
      if(currentAnimation == "DabGauche" && isAnimating == false){
        _animateTo("DabDroite");
      } else if(isAnimating == false){
        _animateTo("DabGauche");
      }
    });
  }

  void _animateTo(String animation) {
    setState(() {
      isAnimating = true;
      currentAnimation = animation;
    });
  }

  void _onLoggedIn(String userId) {
    final databaseReference = dbHandler.getDbReference();
    databaseReference.reference().child(userId).once().then((
        DataSnapshot snapshot) {
      if (snapshot.value != null){
        Map<dynamic, dynamic> map = snapshot.value;
        setState(() {
          _counter = map.values.toList()[0];
          dabPowerClick = map.values.toList()[1];
          isLogged = true;
          _userId = userId;
        });
      } else {
        setState(() {
          _counter = 0;
          dabPowerClick = 1;
          isLogged = true;
          _userId = userId;
        });
      }
      Navigator.pop(
        context,
        true
      );
    });

  }

  _buildExpandableContent(Store store) {
    List<Widget> columnContent = [];

    for (Bonus content in store.contents)
      columnContent.add(
        new ListTile(
          title: new Text(
            content.title + ' :       ' + content.price.toString() + 'dabs',
            style: new TextStyle(fontSize: 18.0),
          ),
          leading: new Icon(store.icon),
          subtitle: new Text(
            'power+' +
                content.dabPowerClick.toString() +
                ' | auto+' +
                content.hitPerSecond.toString(),
            style: new TextStyle(fontSize: 18.0),
          ),
          enabled: _counter >= content.price,
          onTap: () {_addBonus(content);}
        ),
      );

    return columnContent;
  }

  _addBonus(Bonus bonus) {
    setState(() {
      _counter = _counter - bonus.price;
      dabPowerClick += bonus.dabPowerClick;
      bonus.purchased += 1;
    });
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
//      appBar: AppBar(
//        title: Text(widget.title),
//      ),
        body:
        Center(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage("images/background.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'How much DAB you made:',
                    ),
                    Text(
                      '$_counter',
                      style: Theme.of(context).textTheme.display1,
                    ),
                    Text(
                      'Current click bonus:',
                    ),
                    Text(
                      '$dabPowerClick',
                      style: Theme.of(context).textTheme.display1,
                    ),
                    Expanded(
                        child: GestureDetector(
                          onTap: _incrementCounter,
                          child: FlareActor(
                            "images/Dab-v6.flr",
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            animation: currentAnimation,
                            isPaused: false,
                            callback: (string) {
                              isAnimating = false;
                            },
                          ),
                        )
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: stores.length,
                        itemBuilder: (context, i) {
                          return new ExpansionTile(
                              initiallyExpanded: true,
                              title: new Text(stores[i].title, style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),),
                              children: <Widget> [
                                Column(
                                  children: _buildExpandableContent(stores[i]),
                                )
                              ]
                          );
                        },
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 10),
                          child: RaisedButton(
                            onPressed: () {
                              if(!isLogged){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => LoginSignUpPage(auth: new Auth(), onSignedIn: _onLoggedIn)),
                                );
                              } else {
                                logout();
                              }
                            },
                            child: Text(isLogged ? "Logout" : "Login"),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            )
        ),
      ),
    );
  }
}
